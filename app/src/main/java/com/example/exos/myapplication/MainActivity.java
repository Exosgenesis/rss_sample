package com.example.exos.myapplication;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.matshofman.saxrssreader.RssFeed;
import nl.matshofman.saxrssreader.RssItem;
import nl.matshofman.saxrssreader.RssReader;

public class MainActivity extends AppCompatActivity {

    private final static String URL = "http://k.img.com.ua/rss/ru/kyiv.xml";

    //этот сайт открывается у меня только через VPN, поэтому тестить на нем лень
    //коли есть желание на нам затестить, то перекомменте URL
    //private final static String URL = "http://feeds.feedburner.com/sgpress/dIKC";

    private final static String TAG = "RSS Reader";

    private WebView mWebView;
    private View hudLock;
    private Menu optionsMenu;
    private Handler rssHandler = new Handler(Looper.getMainLooper());
    private Pattern imgTagParser = Pattern.compile("<img.*/>");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActivity();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
        setupActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshContent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.airport_menuRefresh) {
            refreshContent();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupActivity() {
        mWebView = (WebView) findViewById(R.id.webview);
        hudLock = findViewById(R.id.hud_lock);
        refreshContent();
    }

    private void lockScreen() {
        hudLock.setVisibility(View.VISIBLE);
        setRefreshActionButtonState(true);
    }

    private void unlockScreen() {
        hudLock.setVisibility(View.GONE);
        setRefreshActionButtonState(false);
    }

    private void loadDataToWebView(String data) {
        mWebView.loadDataWithBaseURL(null, data, "text/html", "en_US", null);
    }

    private String renderPage(String template, HashMap<String, String> params) {
        String rendered = template;
        Set<String> keySet =  params.keySet();

        for (String key : keySet)
            rendered = rendered.replaceAll("%" + key + '%', params.get(key));

        return rendered;
    }

    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem item = optionsMenu.findItem(R.id.airport_menuRefresh);

            if (item != null) {
                if (refreshing) {
                    MenuItemCompat.setActionView(item, R.layout.actionbar_indeterminate_progress);
                } else {
                    MenuItemCompat.setActionView(item, null);
                }
            }
        }
    }

    private String getImgTag(String txt) {
        Matcher matcher = imgTagParser.matcher(txt);
        if (matcher.find())
            return matcher.group();
        return "";
    }

    private String removeAllImgTags(String txt) {
        Matcher matcher = imgTagParser.matcher(txt);
        return matcher.replaceAll("");
    }

    private String formatDate(Date date) {
        if (date == null)
            return "";

        SimpleDateFormat format = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        return format.format(date);
    }

    private void refreshContent() {
        lockScreen();

        new Thread() {
            @Override
            public void run() {
                String error = null;
                RssFeed feed = null;

                try {
                    feed = RssReader.read(new URL(URL));

                } catch (Exception e) {
                    e.printStackTrace();
                    error = e.getMessage();
                }

                rssHandler.post(new NewsUpdater(feed, error));
            }
        }.start();
    }

    private String sv(String s) {
        return s == null ? "" : s;
    }

    private HashMap<String, String> getParams(RssItem rssItem) {
        int txtSz = (int) getResources().getDimension(R.dimen.news_text);
        int txtHg = (int) getResources().getDimension(R.dimen.news_text_height);
        HashMap<String, String> params = new HashMap<>();

        params.put("title", sv(rssItem.getTitle()));
        params.put("textsize", String.valueOf(txtSz));
        params.put("textlineheight", String.valueOf(txtHg));
        params.put("subtitle", removeAllImgTags(sv(rssItem.getDescription())));
        params.put("date", formatDate(rssItem.getPubDate()));
        params.put("img", getImgTag(sv(rssItem.getDescription())));

        //На выбранной мною ленте отсутствует тег yandex:full-text, описанный в тз
        //если нужно затестить на "родной" ленте то нужно
        //перекомментить 2 последующие строки
        params.put("text", sv(rssItem.getAdditional("fulltext")));
        //params.put("text", sv(rssItem.getAdditional("yandex:full-text")));

        return params;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setBarTitle(String title) {
        ActionBar bar = getSupportActionBar();
        if (bar == null) {
            getActionBar().setTitle(title);
        } else {
            bar.setTitle(title);
        }
    }

    private void toast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private class NewsUpdater implements Runnable {
        private RssFeed feed;
        private String error;

        public NewsUpdater(RssFeed feed, String error) {
            this.feed = feed;
            this.error = error;
        }

        @Override
        public void run() {
            unlockScreen();

            if (error != null) {
                toast(getString(R.string.news_get_err) + error);
                return;
            }

            setBarTitle(feed.getTitle());
            ArrayList<RssItem> rssItems = feed.getRssItems();

            if (rssItems.size() == 0) {
                toast(getString(R.string.no_content));
                loadDataToWebView(getString(R.string.no_content));

            } else  {
                HashMap<String, String> params = getParams(rssItems.get(0));
                loadDataToWebView(renderPage(Templates.main, params));
            }
        }
    }
}
