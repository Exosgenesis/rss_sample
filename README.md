﻿Получение списка новостей
=========================

1. **Для получения списка необходимо распарсить rss-ленту.** Адрес потока данных: http://feeds.feedburner.com/sgpress/dIKC

2. **Вывести последнюю новость в ленте.**
Для этого необходимо создать окно с навигационной панелью и компонентом вывода html-содержимого (webView).
На навигационной панели вывести название ленты новостей и разместить кнопку обновления контента.
В webView вывести новость, используя приложенный шаблон вывода новости.
В шаблоне заменить переменные на соответствующие данные из потока новостей (могут быть пустыми)
    + **%title%** - нод `title`,
    + **%subtitle%** - нод `description` -> `CDATA` -> текстовое содержимое,
    + **%date%** - нод `pubDate` в формате `HH:mm dd/MM/YYYY`, 
    + **%img%** - нод `description` -> `CDATA` -> `img src`, 
    + **%text%** - нод `yandex:full-text`

3. **Дополнительные условия:**
    - должна отрабатываться смена ориентации устройства.
    - при сворачивании и разворачивании приложения содержимое контента должно автоматически обновляться, показывая последнюю в ленте новость.
    - при обновлении содержимого должен показываться слой (HUD-заглушка, блокирующая возможность нажатия) с информацией о том, что информация обновляется.


##### Шаблон вывода новостей:
```
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>%title%</title>
<style>
section#main article {
	font-family: Helvetica, Arial, sans-serif;
	color: #000000;
}

section#main article section:first-child  {
	border-bottom: 1px solid #808080;
	padding: 0px 0px 0px 0px;
	margin: 0px 0px 1px 40px;
}

section#main article section:last-child  {
	border-top: 1px solid #d0d0d0;
	margin: 0px 0px 1px 0px;
	padding: 40px 0px 40px 0px;
	margin: 0px 0px 0px 40px;
}

section#main article hgroup {
	float: left;
	max-width: 306px;
	padding: 30px 0px 0px 0px;
}

section#main article img {
	float: right;
	margin: 0px 0px 0px 0px;
	max-width: 400px;
}

section#main article hgroup h1 {
	font-family: Georgia, 'Times New Roman', serif;
	font-size: 25px;
	font-weight: bold;
	margin: 0px 0px 15px 0px;
	min-height: 145px;
}

section#main article hgroup h2 {
	font-size: 20px;
	font-weight: normal;
	margin: 0px 0px 15px 0px;
}

section#main article hgroup h6 {
	font-family: Helvetica, Arial, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #979797;
	margin: 0px 0px 5px 0px;
}

section#main article p {
	font-family: Helvetica, Arial, sans-serif;
	font-size: %textsize%px;
	line-height: %textlineheight%px;
	font-weight: normal;
	max-width: 532px;
	margin: 0px 0px 15px 0px;
	line-height: 150%;
}

.clearfix:before, .clearfix:after { content: ""; display: table; }
.clearfix:after { clear: both; }
.clearfix { *zoom: 1; }

</style>
</head>
<body>
<section id="main">
<article>
<section class="clearfix">
<hgroup>
<h1>%title%</h1>
<h2>%subtitle%</h2>
<h6>%date%</h6>
</hgroup>
%img%
</section>
<section>
<p>%text%</p>
</section>
</article>
</section>
</body>
</html>
```